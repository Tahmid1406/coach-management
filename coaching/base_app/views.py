
from django.shortcuts import redirect, render
from .form import CoachForm, CreateUserForm
from .models import Coach
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .filters import CoachFilter
from django.db.models import Q


def Sidebar(request):
    return render(request, 'base_app/index.html')


def RegisterPage(request):
    return render(request, 'base_app/register_page.html')


def Home(request):
    return render(request, 'base_app/home.html')


def Coaches(request):
    coaches = Coach.objects.all()

    # coach_filter = CoachFilter(
    #     request.GET, queryset=CoachFilter.get_queryset())
    # coaches = coach_filter.qs
    query = request.GET.get('query')

    if query != '' and query is not None:
        qs1 = coaches.filter(specialization__icontains=query)
        qs2 = coaches.filter(about__icontains=query)

        coaches = qs1.union(qs2)

    context = {'coaches': coaches}

    return render(request, 'base_app/coaches.html', context)


def RegisterCoach(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        form = CoachForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Coach profile created for ' + name + '. Please login to continue.', extra_tags='coach-register')
            return redirect('login')
    form = CoachForm()
    return render(request, 'base_app/RegisterCoach.html', {'form': form})


def Login(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            messages.error(
                request, 'Incorrect username or password, try again.', extra_tags='login')
            return redirect('login')

    return render(request, 'base_app/login.html')


def Register(request):
    form = CreateUserForm()

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        username = request.POST.get('username')
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Account created for ' + username + '. Please login to continue. ', extra_tags='user-register')
            return redirect('login')

    context = {'form': form}
    return render(request, 'base_app/register.html', context)


def Logout(request):
    logout(request)
    return redirect('login')
