from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.db import models
from django.forms import fields
from .models import Coach


class CoachForm(forms.ModelForm):
    class Meta:
        model = Coach
        fields = ('name', 'email', 'image', 'specialization', 'about')

        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control cr-input',
                'id': 'form-name',
                'placeholder': 'Enter your full name'}),

            'email': forms.EmailInput(attrs={
                'class': 'form-control cr-input',
                'id': 'form-email',
                'placeholder': 'Enter a valid email'}),


            'specialization': forms.TextInput(attrs={
                'class': 'form-control cr-input',
                'id': 'form-spe',
                'placeholder': 'Field of your specilization'}),

            'about': forms.TextInput(attrs={
                'class': 'form-control cr-input',
                'id': 'form-about',
                'placeholder': 'Write something about yourself'}),

        }

        labels = {
            'image': '',
            'name': '',
        }


class CreateUserForm(UserCreationForm):

    password1 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'form-control rf-input',
                   'type': 'password',
                   'align': 'center',
                   'placeholder': 'Enter a password', })
    )

    password2 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'form-control rf-input',
                   'type': 'password',
                   'align': 'center',
                   'placeholder': 'Confirm your password', })
    )

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

        # password2 = forms.CharField(
        #     label="Confirm password",
        #     widget=forms.PasswordInput(
        #         attrs={'class': 'pass', 'type': 'password', 'align': 'center', 'placeholder': 'password'}),
        # )

        widgets = {
            'username': forms.TextInput(attrs={
                'class': 'form-control rf-input',
                'id': 'username',
                'placeholder': 'Enter your full name'}),


            'email': forms.EmailInput(attrs={
                'class': 'form-control rf-input',
                'id': 'email',
                'placeholder': 'Enter valid email address'}),

        }
