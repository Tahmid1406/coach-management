from django.db.models import fields
import django_filters
from .models import *
from django import forms


class CoachFilter(django_filters.FilterSet):

    name = django_filters.ModelChoiceFilter(queryset=Coach.objects.all(), widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'name', 'type': 'text'}))

    specialization = django_filters.ModelChoiceFilter(queryset=Coach.objects.all(), widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'search specialization', 'type': 'text'}))

    about = django_filters.ModelChoiceFilter(queryset=Coach.objects.all(), widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'search keyword', 'type': 'text'}))

    class Meta:
        model = Coach
        fields = ['name', 'specialization', 'about']
