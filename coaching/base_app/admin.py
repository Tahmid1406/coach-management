from django.contrib import admin
from .models import Coach
# Register your models here.


@admin.register(Coach)
class CoachAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'email', 'image', 'specialization', 'about']
