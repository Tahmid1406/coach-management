from django.urls import path
from django.urls.conf import include
from .views import Logout, Sidebar, Register, Coaches, RegisterCoach, Home, RegisterPage, Login
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', Home, name='home'),
    path('coaches/', Coaches, name='coaches'),
    path('login/', Login, name='login'),
    path('logout/', Logout, name='logout'),
    path('register/', Register, name='register'),
    path('registercoach/', RegisterCoach, name='registercoach'),
    path('registerpage/', RegisterPage, name='registerpage'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
