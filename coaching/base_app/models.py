import django
from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE

# Create your models here.


class Coach(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(max_length=254, blank=False, default="")
    image = models.ImageField(upload_to='coach_image')
    specialization = models.CharField(max_length=100)
    about = models.CharField(max_length=1000)
